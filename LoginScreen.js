/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity
} from "react-native";

import {
  GoogleSignin,
  statusCodes,
  GoogleSigninButton
} from "react-native-google-signin";

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      signedIn: false,
      name: "",
      photo: ""
    };
  }
  componentDidMount() {
    GoogleSignin.configure({
      offlineAccess: false,
      scopes: ["https://www.googleapis.com/auth/drive.readonly"],
      webClientId:
        "832213531060-50llsvuo3dm07v22vm1m8d7i91dtv41f.apps.googleusercontent.com"
    });
    this.getCurrentUserInfo();
  }
  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices({
        showPlayServicesUpdateDialog: true
      });
      const result = await GoogleSignin.signIn();
      console.warn(result.user.name);
      console.warn(result.accessToken);
      this.setState({
        signedIn: true,
        name: result.user.name,
        access_token: result.accessToken
      });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.warn("SIGN_IN_CANCELLED");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        console.warn("IN_PROGRESS");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.warn("PLAY_SERVICES_NOT_AVAILABLE");
      } else {
        // some other error happened
        console.warn(error);
      }
    }
  };
  getCurrentUserInfo = async () => {
    try {
      const result = await GoogleSignin.signInSilently();
      this.setState({
        signedIn: true,
        name: result.user.name,
        access_token: result.accessToken
      });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        // user has not signed in yet
        // this.signIn();
      } else {
        // some other error
        alert("some other error");
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logocontainer}>
          <Image
            source={{ uri: "logo" }}
            resizeMode={"contain"}
            fadeDuration={0}
            style={styles.logo}
          />
        </View>
        <View style={styles.logincontainer}>
          <Text style={styles.welcome}>Sign in with Google</Text>
          <TouchableOpacity
            accessible={true}
            accessibilityLabel="sign in with google account"
            onPress={this.signIn.bind(this)}
            style={styles.logintouch}
          >
            <Image
              source={{ uri: "login" }}
              resizeMode={"contain"}
              fadeDuration={0}
              style={styles.login}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  logocontainer: {
    marginTop: 30,
    width: "100%",
    height: "40%",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  logincontainer: {
    width: "100%",
    height: "60%",
    alignItems: "center",
    justifyContent: "flex-start"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 50
  },
  logo: {
    width: "60%",
    height: 80,
    flex: 1
  },
  logintouch: {
    flex: 1,
    width: "100%",
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  login: {
    width: "60%",
    height: 80,
    flex: 1,
    alignItems: "center"
  }
});

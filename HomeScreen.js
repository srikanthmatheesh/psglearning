/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
const { height, width } = Dimensions.get("window");

import { GoogleSignin, statusCodes } from "react-native-google-signin";
import FCM, { NotificationActionType } from "react-native-fcm";
import { createDrawerNavigator, createStackNavigator } from "react-navigation";

export default class HomeScreen extends Component {
  componentDidMount() {
    GoogleSignin.configure({
      offlineAccess: false,
      scopes: ["https://www.googleapis.com/auth/drive.readonly"],
      webClientId:
        "832213531060-50llsvuo3dm07v22vm1m8d7i91dtv41f.apps.googleusercontent.com"
    });
    this.getCurrentUserInfo();
	
    FCM.getFCMToken().then(token => {
      try {
        if (token !== null) {
          this.setState({
            notification_token: token
          });
        }
      } catch (error) {
        console.warn(error);
      }
    });
  }
  signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();
      this.setState({ user: null }); // Remember to remove the user from your app's state as well
    } catch (error) {
      console.error(error);
    }
  };
  signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices({
        showPlayServicesUpdateDialog: true
      });
      const result = await GoogleSignin.signIn();
      console.warn("Name:", result.user.name);
      console.warn("AccessToken:", result.accessToken);
      console.warn("NotificationToken:", this.state.notification_token);
      this.setState({
        signedIn: true,
        name: result.user.name,
        access_token: result.accessToken
      });
      this.props.navigation.navigate("Category", {
        name: result.user.name,
        access_token: result.accessToken,
        notification_token: this.state.notification_token
      });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.warn("SIGN_IN_CANCELLED");
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
        console.warn("IN_PROGRESS");
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.warn("PLAY_SERVICES_NOT_AVAILABLE");
      } else {
        // some other error happened
        console.warn(error);
      }
    }
  };
  getCurrentUserInfo = async () => {
    try {
      const result = await GoogleSignin.signInSilently();
      this.setState({
        signedIn: true,
        name: result.user.name,
        access_token: result.accessToken,
        notification_token: this.state.notification_token
      });

      this.props.navigation.navigate("Category", {
        name: result.user.name,
        access_token: result.accessToken,
        notification_token: this.state.notification_token
      });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_REQUIRED) {
        // user has not signed in yet
        // this.signIn();
      } else {
        // some other error
        alert("some other error");
      }
    }
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logocontainer}>
          <Image
            source={require("./img/logo.png")}
            resizeMode={"contain"}
            fadeDuration={0}
            style={styles.logo}
          />
        </View>
        <View style={styles.logincontainer}>
          <Text style={styles.welcome}>Sign in with Google</Text>
          <TouchableOpacity
            accessible={true}
            accessibilityLabel="sign in with google account"
            onPress={this.signIn.bind(this)}
            style={styles.logintouch}
          >
            <Image
              source={require("./img/login.png")}
              resizeMode={"contain"}
              fadeDuration={0}
              style={styles.login}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  logocontainer: {
    marginTop: 30,
    width: width - 100,
    height: "40%",
    alignItems: "center",
    justifyContent: "center"
  },
  logincontainer: {
    width: width,
    height: "60%",
    alignItems: "center",
    justifyContent: "center"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 50
  },
  logo: {
    width: width - 200,
    height: 80,
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  },
  logintouch: {
    flex: 1,
    width: width,
    height: 50,
    alignItems: "center",
    justifyContent: "center"
  },
  login: {
    width: "60%",
    height: 40,
    flex: 1,
    alignItems: "center"
  }
});

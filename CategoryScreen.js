/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, Alert, Button } from "react-native";
import { TagSelect } from "react-native-tag-select";

export default class CategoryScreen extends Component {
  constructor(props) {
    super(props);
    this.notification_token = "";
    this.access_token = "";
    this.name = "";
    this.topic_id = "";
    this.selected = [];
    this.state = {
      topics: [],
      isloaded: false,
      notification_token: "",
      access_token: "",
      name: ""
    };
  }
  componentDidMount() {
    fetch("http://192.168.43.148:8000/api/v1/topics", {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        // AsyncStorage.setItem("device_token", responseJson.device_token)
        //   .then(() => {})
        //   .catch(() => {
        //     Snackbar.show({
        //       title:
        //         "Notification: Unable to regiter device token. Please re-install the app",
        //       duration: Snackbar.LENGTH_SHORT
        //     });
        //   });
        this.setState({
          isloaded: true,
          topics: responseJson.results
        });
        console.warn(responseJson);
      })
      .catch(e => e.message);
  }

  _signUp = async () => {
    this.topic_id = this.tag.itemsSelected[0].id;

    console.warn("NotificationToken:", this.notification_token);
    console.warn("AccessToken:", this.access_token);
    console.warn("Name:", this.name);
    console.warn("Topic:", this.topic_id);

    let str = [];
    str.push("notification_token=" + encodeURIComponent(this.notification_token));
    str.push("auth_token=" + encodeURIComponent(this.access_token));
    str.push("name=" + encodeURIComponent(this.name));
    str.push("topic_id=" + (this.topic_id));

    let formBody = str.join("&");

	console.warn(formBody);

	fetch("http://192.168.43.148:8000/api/v1/registration", {
      method: "POST",
      body: formBody,
      headers: {
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      }
    })
      .then(response => response.json())
      .then(responseJson => {
        console.warn(responseJson);
      })
      .catch(error => {
        console.error(error);
      });
  };

  render() {
    const { access_token, notification_token, name, navigation } = this.props;
    const { state } = navigation;
    this.access_token = state.params.access_token;
    this.notification_token = state.params.notification_token;
    this.name = state.params.name;
    return (
      <View style={styles.container}>
        {this.state.isloaded == true ? (
          <View>
            <Text style={styles.welcome}>Choose a category to proceed</Text>
            <TagSelect
              data={this.state.topics}
              itemStyle={styles.item}
              itemLabelStyle={styles.label}
              itemStyleSelected={styles.itemSelected}
              itemLabelStyleSelected={styles.labelSelected}
              max={1}
              ref={tag => {
                this.tag = tag;
              }}
              onMaxError={() => {
                Alert.alert("Ops", "Max reached");
              }}
            />

            <Button
              title="Complete Signup"
              onPress={() => {
                this._signUp();
              }}
            />
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    marginTop: 50
  },
  labelText: {
    color: "#333",
    fontSize: 15,
    fontWeight: "500",
    marginBottom: 15
  },
  item: {
    borderWidth: 1,
    borderColor: "#333",
    backgroundColor: "#FFF"
  },
  label: {
    color: "#333"
  },
  itemSelected: {
    backgroundColor: "#333"
  },
  labelSelected: {
    color: "#FFF"
  }
});
// <Button
//   title="Complete Signup"
//   onPress={() => this.props.navigation.navigate("Home")}
// />

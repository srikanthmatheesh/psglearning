/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ActivityIndicator
} from "react-native";
const { height, width } = Dimensions.get("window");

export default class SplashScreen extends Component {
  render() {
    return (
      <View style={styles.container}>
		<Image
		  source={{uri:"logo"}}
		  resizeMode={"contain"}
		  fadeDuration={0}
		  style={styles.logo}
		/>
        <ActivityIndicator animating={true} size="large" color="blue" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
     flex: 1,
	 alignItems: "center",
	 justifyContent: "center"
  },
  logo: {
	width: width - 200,
	height: 80
  }
});

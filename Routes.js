/**
 * @flow
 */

import React from "react";
import { createStackNavigator, createAppContainer } from "react-navigation";

import HomeScreen from "./HomeScreen";
import LoginScreen from "./LoginScreen";

const AppNavigator = createStackNavigator(
  {
    Home: { screen: HomeScreen },
    Login: { screen: LoginScreen }
  },
  {
    initialRouteName: "Home"
  }
);

export default createAppContainer(AppNavigator);

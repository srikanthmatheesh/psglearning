/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Alert,
  Button,
  AsyncStorage
} from "react-native";
import BackgroundTask from "react-native-background-task";
var PushNotification = require("react-native-push-notification");

PushNotification.configure({
  onNotification: function(notification) {
    console.warn("NOTIFICATION:", notification);
  },
  senderID: "832213531060",
  popInitialNotification: true,
  requestPermissions: true
});

BackgroundTask.define(async () => {
  const response = await fetch("http://192.168.43.148:8000/api/v1/posts");
  const text = await response.text();

  await AsyncStorage.setItem("posts", posts);
  [...this.state.posts, ...posts];
  BackgroundTask.finish();
});

export default class CategoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
      isloaded: false
    };
  }
  componentDidMount() {
    BackgroundTask.schedule({
      period: 1800 // run every 30 mins
    });
    this.checkStatus();
  }
  async checkStatus() {
    const status = await BackgroundTask.statusAsync();

    if (status.available) {

	  // Everything's fine
      PushNotification.localNotificationSchedule({
        message: "My GCM Notification Message",
        date: new Date(Date.now() + 60 * 1000)
      });

      return;
    }

    const reason = status.unavailableReason;
    if (reason === BackgroundTask.UNAVAILABLE_DENIED) {
      Alert.alert(
        "Denied",
        'Please enable background "Background App Refresh" for this app'
      );
    } else if (reason === BackgroundTask.UNAVAILABLE_RESTRICTED) {
      Alert.alert(
        "Restricted",
        "Background tasks are restricted on your device"
      );
    }
  }
  render() {
    return (
      <View style={styles.container}>
        {this.state.isloaded == true ? <View /> : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
